# Clean Code and Software Craftsmanship Best Practices

(This list is not exhaustive — feel free to complete it!)

[[_TOC_]]

# I — Well-known principles

## Agile Manifesto & Manifesto for Software Craftsmanship

| Agile Manifesto | Manifesto for Software Craftsmanship |
|---|---|
| Source: https://agilemanifesto.org | Source: https://manifesto.softwarecraftsmanship.org |
| - _Working software_ over comprehensive documentation<br>- _Responding to change_ over following a plan<br>- _Individuals and interactions_ over processes and tools<br>- _Customer collaboration_ over contract negotiation | - Not only working software, but also _well-crafted software_<br>- Not only responding to change, but also _steadily adding value_<br>- Not only individuals and interactions, but also _a community of professionals_<br>- Not only customer collaboration, but also _productive partnerships_ |

## Object-Oriented Programming

### Main concepts

Source: https://info.keylimeinteractive.com/the-four-pillars-of-object-oriented-programming

 - **Encapsulation:**
    - Each object maintains a private state, inside its class, which it manages itself, and which no other object can alter (unless explicitly allowed)
    - Each object exposes a list of publicly accessible functions (aka methods) and constants that other objects can use.
    - Also: it means put together in the same class methods and fields that have a strong semantic cohesion between each other.
 - **Abstraction:**
    - Abstraction is an extension of encapsulation.
    - Abstraction generalizes what is specific.
    - Abstraction acts as a layer of compatibility between two layers of implementation which communicate with each other with only those API endpoints which are relevant to their common need/behavior/responsability/business logic.
    - Abstraction does not contain any concretely implemented business logic — which is fully delegated to the implementation layer.
 - **Inheritance:**
    - Inheritance is the ability of one object (child) to acquire some/all fields of another object (parent), which favors reusability.
 - **Polymorphism:**
    - static p. (compile-time p.), aka overriding, allows a class to have methods which have the same name but a different set of input parameters or a different return value, in a way there is no confusion with mixing types
    - dynamic p. (runtime p.) allows objects of different classes to have methods with exactly the same signature (same name, same input params, same return value), in a way there is no confusion with mixing types

### 4 rules of simple design

Kent Beck came up with his four rules of simple design while he was developing ExtremeProgramming in the late 1990's.

| Source: Kent Beck's original _White Book_, 1st ed. | Source: https://martinfowler.com/bliki/BeckDesignRules.html | Source: https://blog.jbrains.ca/permalink/the-four-elements-of-simple-design |
|---|---|---|
| - Runs all the tests<br>- Has no duplicated logic. Be wary of hidden duplication like parallel class hierarchies<br>- States every intention important to the programmer<br>- Has the fewest possible classes and methods | - Passes the tests<br>- Reveals intention<br>- No duplication<br>- Fewest elements | 1. Passes its tests<br>2. Minimizes duplication<br>3. Maximizes clarity<br>4. Has fewer elements |

### “Premature optimization is the root of all evil”

This famous quote by Sir Tony Hoare (popularized by Donald Knuth) means that, [although optimization is important _per se_](https://ubiquity.acm.org/article.cfm?id=1513451), it should not take place too early in the software product life cycle.

So here are a few other well-known principles to remember and follow:
* [K.I.S.S.](https://en.wikipedia.org/wiki/KISS_principle) — “Keep It Simple, Stupid!”
* [Y.A.G.N.I.](https://en.wikipedia.org/wiki/You_aren%27t_gonna_need_it) — “You Aren't Gonna Need It”

### Clean Code

#### Robert C. Martin's _Clean Code_ summaries

* https://github.com/tum-esi/common-coding-conventions
* https://gist.github.com/wojteklu/73c6914cc446146b8b533c0988cf8d29
* https://gist.github.com/cedrickchee/55ecfbaac643bf0c24da6874bf4feb08
* https://erik-uus.gitbook.io/clean-code/
* https://damien.pobel.fr/post/clean-code/
* https://thewolfsound.com/how-to-write-good-code-lessons-learned-from-clean-code/

#### Code smells

||||
|---|---|---|
| ❌ Duplication | ➔ | <span style="color:LimeGreen">✔️</span> Encapsulate repeated code to avoid duplication |
| ❌ Method or Class too long, or doing too many things | ➔ | <span style="color:LimeGreen">✔️</span> Split them into small, generic, reusable layers of abstraction |
| ❌ Method takes too many arguments | ➔ | <span style="color:LimeGreen">✔️</span> Refactor so that the method takes one object containing many fields |
| ❌ No comments or too many comments | ➔ | <span style="color:LimeGreen">✔️</span> Comments must be relevant and add insight, not be obvious or redundant |
| ❌ Magic numbers / Magic strings | ➔ | <span style="color:LimeGreen">✔️</span> Avoid using out-of-nowhere literals, only use declared variables with understandable names |
| ❌ Global variables | ➔ | <span style="color:LimeGreen">✔️</span> Avoid using global variables, unless they are constants |
| ❌ Bad naming | ➔ | <span style="color:LimeGreen">✔️</span> Name your variables and methods explicitly. Respect [naming conventions](#code-style-standardisez-les-conventions-de-nommage) (see below) |

#### Why? (Benefits)

* Reduced cost
* Improved readability
* More adapted to change
* Less bugs
* Better testability
* Increased motivation

### Object Calisthenics

Source: https://williamdurand.fr/2013/06/03/object-calisthenics/

Object Calisthenics are programming exercises \[the word _calisthenics_ comes from the ancient Greek words kállos (κάλλος), “beauty”, and sthenos (σθένος), “strength”\], formalized as **a set of 9 rules** invented by Jeff Bay in his book _The ThoughtWorks Anthology_:

1. Only One Level Of Indentation Per Method
1. Don't Use The `else` Keyword
1. Wrap All Primitives And Strings
1. First Class Collections
1. One Dot Per Line
1. Don't Abbreviate
1. Keep All Entities Small
1. No Classes With More Than Two Instance Variables
1. No Getters/Setters/Properties

### S. O. L. I. D. principles

Source: https://en.wikipedia.org/wiki/SOLID

- **`S`ingle Responsibility Principle:** a class should have only one reason to change
- **`O`pen/Closed Principle:** a class should be open for extension, but closed for modification
- **`L`iskov Substitution Principle:** objects of a superclass should be replaceable with objects of its subclasses without breaking the application
- **`I`nterface Segregation Principle:**
    * (a) a client software should not be forced to implement an interface it does not use, 
    * (b) a client software should not be forced to depend on methods it does not use
- **`D`ependency Inversion Principle:**
    * (a) high-level modules should not depend on low-level modules; both should depend on abstractions
    * (b) abstractions should not depend on details; details should depend on abstractions

### Law of Demeter, aka Principle of Least Knowledge

Source: https://en.wikipedia.org/wiki/Law_of_Demeter#In_object-oriented_programming

>>>
The law dates back to 1987 when it was first proposed by Ian Holland, who was working on the Demeter Project — the birthplace of a lot of Aspect Oriented Programming (AOP) principles. 

(...)

An object `A` can request a service (call a method) of an object instance `B`, but object `A` should not "reach through" object `B` to access yet another object, `C`, to request its services. 

Doing so would mean that object `A` implicitly requires greater knowledge of object `B`'s internal structure.

(...)

Instead, `B`'s interface should be modified if necessary so it can directly serve object `A`'s request, propagating it to any relevant subcomponents.

Alternatively, `A` might have a direct reference to object `C` and make the request directly to that. If the law is followed, only object `B` knows its own internal structure.

(...)

In particular, an object should avoid invoking methods of an object returned by another method. For many modern object oriented languages that use a dot as field identifier, the law can be stated simply as "use only one dot". That is, the code `a.m().n()` breaks the law where `a.m()` does not. As an analogy, when one wants a dog to walk, one does not command the dog's legs to walk directly; instead one commands the dog which then commands its own legs. 
>>>

### “Tell, don't ask!”

Source: https://martinfowler.com/bliki/TellDontAsk.html

<img src="https://martinfowler.com/bliki/images/tellDontAsk/sketch.png" width="50%">

❌ Bad: the logic asks AskMonitor to provide so many of its fields
```java
AskMonitor am = new AskMonitor("Time Vortex Hocus", 2, alarm);
am.setValue(3);
if (am.getValue() > am.getLimit())
  am.getAlarm().warn(am.getName() + " too high");
```

✔️ Better: move that logic into an appropriate method inside the monitor class
```java
TellMonitor tm = new TellMonitor("Time Vortex Hocus", 2, alarm);
tm.setValue(3);
⋮
class TellMonitor() {
⋮
  public void setValue(int arg) {
    value = arg;
    if (value > limit)
      alarm.warn(name + " too high");
  }
⋮
}
```

Purpose & Pros:

* ✔️ Helps increase co-location of data and behavior.

Limits & Cons:

* ❌ Seeking to comply with the fundamental principle of co-locating data and behavior should sometimes be dropped in favor of other concerns — such as layering. Good design is all about trade-offs, and co-locating data and behavior is just one factor to bear in mind.

### Design Patterns

* Design patterns are clever thoughtful ways of organizing object classes in order to prevent (or cure) cases of a heavy technical debt (typically a counterproductively increasing number of either object classes, or their complexity, or both) by ensuring compliance with the S.O.L.I.D. principles, loose coupling, good scalability, and more.
* The 23 classic software design patterns were first discussed in the influential book _Design Patterns: Elements of Reusable Object-Oriented Software_ (1994) written by Erich Gamma, Richard Helm, Ralph Johnson, and John Vlissides, often referred to as the Gang of Four (GoF).

#### “Gang of Four” Design Patterns catalog

Source: https://refactoring.guru/design-patterns/catalog

#### Creational Patterns

Patterns which provide various object creation mechanisms which increase flexibility and reuse of existing code

##### Factory Method (aka Virtual Constructor)

Purpose & Pros:

* ✔️ It is relevant to implement when business logic deals with creating objects of different types but still similar enough (ie with a potential to implement the same interface).
* ✔️ Defines a common interface which commands the created objects to be typed as their superclass (Dependency Inversion Principle), while allowing for the type to be overridden by subclasses (Liskov Substitution Principle).
* ✔️ Object construction code is separated from the business logic code (Single Responsibility Principle), thus making code base easier to support.
* ✔️ Loose coupling between the creator class and the product variant classes makes the code base easier to extend (Open/Closed Principle): add new types of creator or new product variants without breaking existing client code.

Limits & Cons:

* ❌ May result in large parallel class hierarchies (code smells!), since each product class must have a corresponding creator subclass.

##### Abstract Factory

Purpose & Pros:

* ✔️ It is relevant to implement when business logic deals with a matrix of distinct product types versus variants of these products.
* ✔️ It provides same advantages as a Factory Method, but also an additional abstraction layer allowing to instantiate and manage a pool of similar but specialized factory objects, each one in charge of creating a similar but distinct family of product objects; _similar_ for each family must implement the same interface, but _distinct_ as a variant.

Limits & Cons:

* ❌ Adds a slew of additional classes and interfaces to the code base.
* ❌ Requires each variant to have as many product types as stated in the interface.

##### Builder

Purpose & Pros:

* ✔️ It is relevant to implement when business logic requires classes of objects to have either (a) a lot of constructors (eg in order to satisfy different possibilities of combinations of input parameters), or (b) constructors with a lot of input parameters (eg a complex object with many fields and nested objects), or both.
* ✔️ You need to create builder classes responsible for the step-by-step creation of different representations of the product object, and have them implement a common interface (base builder). If creation steps have to be executed in a particular order, you may also need a director class to handle that. A builder object carries a stub of a product object, progressively fills in its fields, finally returns it, and resets the stub.

Limits & Cons:

* ❌ The overall complexity of the code increases since the pattern requires creating multiple new classes.
* ❌ The client will be bound to specific builder classes, since the director's interface may not have a method to return the finished product object.

##### Prototype (aka Clone)

Purpose & Pros:

* ✔️ It is relevant to implement when business logic deals with objects that have dozens of fields and hundreds of possible configurations, and cloning those might serve as an alternative to subclassing.
* ✔️ For this DP to work, all prototype objects —objects expected to be cloned— need to implement an interface containing a single `clone()` method, so that they are able —by themselves— to create exact copies of themselves, including private fields.
* ✔️ As an easy way to access frequently-used prototypes, you may also want to implement a prototype registry class, which stores (eg in a hash map) a set of pre-built objects that are ready to be copied.

Limits & Cons:

* ❌ Cloning complex objects that have circular references might be very tricky.

##### Singleton

Purpose & Pros:

* ✔️ Although controversial and not recommended, this DP may be relevant to implement when business logic needs to control access to a shared writeable resource (eg a database or a file), often within a multithreaded environment, by making sure only one single instance of a single class is given that access, and that any subsequent attempt to instantiate that object will be bound to return the already existing instance. On the other hand, the singleton instance itself must be accessible by any other object that needs to call it.

Limits & Cons:

* ❌ Violates the Single Responsibility Principle. The pattern solves two problems at the same time: (1) Ensure that a class has just a single instance, and (2) Provide a global access point to that instance.
* ❌ The Singleton pattern can mask bad design, for instance, when the components of the program know too much about each other (violation of the Law of Demeter!).
* ❌ The pattern requires special treatment in a multithreaded environment so that multiple threads won't create a singleton object several times.
* ❌ It may be difficult to unit test the client code of the Singleton because many test frameworks rely on inheritance when producing mock objects. Since the constructor of the singleton class is private and overriding static methods is impossible in most languages, you will need to think of a creative way to mock the singleton. Or don't write the tests. Or don't use the Singleton pattern altogether.

#### Structural Patterns

Patterns which explain how to assemble objects and classes into larger structures while keeping these structures flexible and efficient

##### Adapter (aka Wrapper)

Purpose & Pros:

* ✔️ It is relevant to implement when business logic needs to deal with one or more existing (legacy or third-party) classes that either lack some common functionality that cannot be added to their superclass, or their interface is incompatible with the “regular” classes that need to deal with them.
* ✔️ The Adapter pattern ensures the adaptee classes' compatibility with the “regular” classes, by either extending one of the compatible classes into an adapter, or implementing a relevant interface; and then redefining a supported method to provide the needed conversion logic.
* ✔️ This respects the Single Responsability Principle, spares the developer having to modify the existing classes or to provide each of them with a new feature (Open/Closed Principle violation! Code duplication!)

Limits & Cons:

* ❌ The overall complexity of the code increases because you need to introduce a set of new interfaces and classes. Sometimes it's simpler just to change the service class so that it matches the rest of your code.

##### Bridge

Purpose & Pros:

* ✔️ It is relevant to implement when business logic deals with an extensible class hierarchy which evolves in two or more related but independent dimensions simultaneously (eg abstraction/platform, domain/infrastructure, front-end/back-end, or interface/implementation). This is violation of the Single Responsibility Principle, and it results in an exponentially growing number of subclasses (as it is the Cartesian product of elements composing the dimensions).
* ✔️ The Bridge pattern separates dimensions by extracting each one into a separate class hierarchy (Single Responsibility Principle; Open/Closed Principle). One class hierarchy is typically called _abstraction_ (eg the UI), while the other _implementation_ (eg the platform-specific configuration). The base class of the abstraction hierarchy must define the operations needed by the client, which will be adopted by more specific (or refined) abstractions. While the implementation hierarchy must follow a common interface which commands each class to expose only features relevant to the client.
* ✔️ The Bridge pattern switches from inheritance to composition (loose coupling): the base abstraction class contains a reference field to the implementation interface (Dependency Inversion principle), and the client passes an implementation object to the abstractions constructor, hence bypassing the need to deal with the implementation object, only with that of the abstraction.

Limits & Cons:

* ❌ You might make the code more complicated by applying the pattern to a highly cohesive class.

##### Composite (aka Object Tree)

Purpose & Pros:

* ✔️ It is relevant to implement when the core model of your app can be represented as a tree with multiple layers of complex objects (aka containers) containing one-to-many both other complex objects and simple objects (aka leaves), with the client requesting to recursively process through the tree and return a result.
* ✔️ Both containers and leaves classes must implement a common interface with a list of methods that make sense for both of them.
* ✔️ Behind the same methods, the implementation differs with the leaves returning something, while the containers will delegate processing to their sub-components, process intermediate results, and then return the final result to the client or the caller container.
* ✔️ The interface allows the client to treat any object as a tree, without having to know whether it is a leaf or a container, or how deeply nested it is (loose coupling; Dependency Inversion Principle).

Limits & Cons:

* ❌ It might be difficult to provide a common interface for classes whose functionality differs too much. In certain scenarios, you'd need to overgeneralize the component interface, making it harder to comprehend.

##### Decorator (aka Wrapper)

Purpose & Pros:

* ✔️ It is relevant to implement when the application features a component whose base behavior needs to be enhanced with multiple options at runtime (eg save a file + compress it + encrypt it + create a backup copy) with the possibility to process them before or after the base behavior, as well as to combine the options.
* ✔️ Same as in the [Composite](#composite-aka-object-tree) DP, both the component class (the one with the basic behavior) and the decorators class hierarchy need to implement a common interface. Every decorator class extends a base decorator class which has a field for referencing a wrapped component object; the field is typed as the common interface so it can contain both concrete components and decorators in order to be 
    * ✔️ interchangeable from the client's perspective (Dependency Inversion Principle), 
    * ✔️ combinable: the client can wrap components in multiple layers of decorators (wrappers stack) provided they support the common interface.
* ✔️ While the base decorator delegates all operations to the wrapped object, its child decorator classes override its methods to define extra behavior (same as hooks in [Template Method](#template-method)) that can be added to components dynamically either before or after calling the parent method.
* ✔️ Use the pattern when it's awkward or not possible to extend an object's behavior using inheritance. Many programming languages have the `final` keyword that can be used to prevent further extension of a class. For a final class, the only way to reuse the existing behavior would be to wrap the class with your own wrapper, using the Decorator pattern.

Limits & Cons:

* ❌ It may be hard to unwrap (take off) a nested wrapper from the wrappers stack.
* ❌ It is hard to implement a decorator in such a way that its behavior doesn't depend on the order in the wrappers stack.
* ❌ The initial configuration code of layers might look pretty ugly.

Relations with Other Patterns:

* 🔀 While the [Adapter](#adapter-aka-wrapper) DP changes the interface of an existing object, the Decorator enhances an object without changing its interface. In addition, Decorator supports recursive composition, which isn't possible when you use Adapter.
* 🔀 The [Composite](#composite-aka-object-tree) and Decorator have similar structure diagrams since both rely on recursive composition to organize an open-ended number of objects. Unlike the Composite DP the Decorator only has one child component. Also, the Decorator adds additional responsibilities to the wrapped object, while Composite just “sums up” its children's results. However, the patterns can also cooperate: you can use Decorator to extend the behavior of a specific object in the Composite tree.
* 🔀 In case the order of execution of decorators matters (eg first compress, then encrypt, then save, then save a backup copy), the Decorator can be combined with the [Builder](#builder) DP and its Director object.

##### Facade

Purpose & Pros:

* ✔️ It is relevant to implement when your app needs to integrate with a sophisticated subsystem or a third-party library that has dozens of features (eg the `ffmpeg` video processor) while all you need is a straightforward interface to only a limited number of its functionalities. In other words, you need to isolate your code from the complexity of a subsystem.
* ✔️ Using more than one facade can help structure a complex subsystem into layers by defining entry points to each level of a subsystem; or to reduce coupling between multiple subsystems by requiring them to communicate only through facades. For example, a complex video processing framework can be broken down into two layers: video- and audio-related. For each layer, you can create a facade and then make the classes of each layer communicate with each other via those facades. This approach looks very similar to the [Mediator](#mediator) DP.

Limits & Cons:

* ❌ A facade can become a god object coupled to all classes of an app.

Relations with Other Patterns:

* 🔀 Facade is similar to [Proxy](#proxy) in that both buffer a complex entity and manage its life cycle. Unlike Facade, Proxy has the same interface as its service object, which makes them interchangeable.

##### Flyweight

Purpose & Pros:

* ✔️ It is relevant to implement when either (a) your application needs to spawn a huge number of similar objects, or (b) your application drains all available RAM on a target device, or (c) the objects contain duplicate states which can be extracted and shared: one shareable (aka intrinsic) and one specific (aka extrinsic). You can save lots of RAM, assuming your program has tons of similar objects.

Limits & Cons:

* ❌  The code becomes much more complicated. New team members will always be wondering why the state of an entity was separated in such a way.
* ❌  You might be trading RAM over CPU cycles as some of the context data will have to be resolved each time somebody calls a flyweight method.

Relations with Other Patterns:

* 🔀 You can implement shared leaf nodes of the [Composite](#composite-aka-object-tree) tree as Flyweights to save some RAM.
* 🔀 Flyweight shows how to make lots of little objects, whereas [Facade](#facade) shows how to make a single object that represents an entire subsystem.
* 🔀 Flyweight would resemble [Singleton](#singleton) if you somehow managed to reduce all shared states of the objects to just one flyweight object. But there are two fundamental differences between these patterns:
    * There should be only one Singleton instance, whereas a Flyweight class can have multiple instances with different intrinsic states.
    * The Singleton object can be mutable. Flyweight objects are immutable.

##### Proxy

Purpose & Pros:

* ✔️ A proxy may be necessary to set up in a few use cases:
    * ✔️ When you need to control the lifecycle of a heavyweight but rarely needed service object so that it does not waste system resources by being always up. The proxy can keep track of who was given a reference to the service object, check whether those who were are still active — otherwise unplug the service object and free the system resources.
    * ✔️ When you want only the clients with specific credentials to be granted access to the service object.
    * ✔️ When you want to simplify the client's reaching out to a remote service.
    * ✔️ When you need to alleviate a highly requested remote service with a mechanism of load balancing (redirecting client's request from a busy node to an available one) 
    * ✔️ When you need something to be done along with every request to the service object (eg log a history of requests).
    * ✔️ When you need to resolve from which data source to fetch a requested data (eg main database or in-memory cache). The Proxy DP can look up the in-memory cache for already created objects in order not to disturb the DB, as well as decide whether yet another object should be cached.
* ✔️ The Proxy DP suggests that you create a new proxy class with the same interface as an original service object. Then you update your app so that it passes the proxy object to all of the original object’s clients. Upon receiving a request from a client, the proxy delegates the job to the original service object.

Limits & Cons:

* ❌ The code may become more complicated since you need to introduce a lot of new classes.
* ❌ The response from the service might get delayed.

Relations with Other Patterns:

* 🔀 [Decorator](#decorator-aka-wrapper) and Proxy have similar structures, but very different intents. Both patterns are built on the composition principle, where one object is supposed to delegate some of the work to another. The difference is that a Proxy usually manages the life cycle of its service object on its own, whereas the composition of Decorators is always controlled by the client.
* 🔀 While Proxy provides the wrapped object with the same interface, [Decorator](#decorator-aka-wrapper) provides it with an enhanced interface, and [Adapter](#adapter-aka-wrapper) provides it with a different interface.
* 🔀 [Facade](#facade) is similar to Proxy in that both buffer a complex entity and manage its life cycle. Unlike Facade, Proxy has the same interface as its service object, which makes them interchangeable.

#### Behavioral Patterns

Patterns which are concerned with algorithms and the assignment of responsibilities between objects

##### Chain of Responsibility

Purpose & Pros:

* ✔️ _

Limits & Cons:

* ❌ _

##### Command

Purpose & Pros:

* ✔️ _

Limits & Cons:

* ❌ _

##### Iterator

Purpose & Pros:

* ✔️ _

Limits & Cons:

* ❌ _

##### Mediator

Purpose & Pros:

* ✔️ _

Limits & Cons:

* ❌ _

##### Memento

Purpose & Pros:

* ✔️ _

Limits & Cons:

* ❌ _

##### Observer

Purpose & Pros:

* ✔️ It is relevant to implement when your business logic deals with observee/observer (or publisher/subscriber) relationships between objects at runtime, with changes to the state of one object (observee aka publisher aka model) triggering some behavior of other objects (observers aka subscribers), without implementing brute force overkills like the observers having to constantly check for a state change of the observee, or the observee pointlessly propagating notifications to every living object just to make sure everyone has got the news. GUI apps are a typical example of this context, when a change in state induced by the user needs to trigger immediate visual response. Another example is low-level logging with a mechanism that enables objects to produce technical logs automatically.
* ✔️ Observer DP enables communication between loosely coupled objects (Open/Closed Principle), with any object that implements the subscriber interface posed to be subscribed for event notifications from one or more publisher objects; and vice-versa, the publisher objects (or their event manager objects) spreading notifications only to their subscribers.
* ✔️ The subscription list is dynamic, with the publisher interface defining methods for adding and removing subscribers at runtime, as some objects in your app may need to observe others only for a limited time or in specific cases.

Limits & Cons:

* ❌ Subscribers are notified in random order.

Relations with Other Patterns:

* 🔀 If you want an existing class hierarchy to become publishers but without adding new code to those classes (Open/Closed Principle), you can wrap them in ad-hoc [Decorators](#decorator-aka-wrapper) which will trigger the notification mechanism every time something important happens about the publisher.

##### State

Purpose & Pros:

* ✔️ _

Limits & Cons:

* ❌ _

##### Strategy

Purpose & Pros:

* ✔️ Strategy DP is relevant to implement when your business logic deals with algorithms (sorting, search, filtering, mapping):
    * ✔️ Use the Strategy pattern when you want to use different variants of an algorithm within an object and be able to switch from one algorithm to another during runtime.
    * ✔️ Use the Strategy when you have a lot of similar classes that only differ in the way they execute some behavior: The Strategy pattern lets you extract the varying behavior into a separate class hierarchy and combine the original classes into one, thereby reducing duplicate code.
    * ✔️ Use the pattern to isolate the business logic of a class from the implementation details of algorithms that may not be as important in the context of that logic.
    * ✔️ Use the pattern when your class has a massive conditional statement that switches between different variants of the same algorithm.
* ✔️ You can swap algorithms used inside an object at runtime.
* ✔️ You can isolate the implementation details of an algorithm from the code that uses it.
* ✔️ You can replace inheritance with composition.
* ✔️ You can introduce new strategies without having to change the context (Open/Closed Principle).

Limits & Cons:

* ❌ A lot of modern programming languages have functional type support that lets you implement different versions of an algorithm inside a set of anonymous (aka lambda) functions. Then you could use these functions exactly as you’d have used the strategy objects, but without bloating your code with extra classes and interfaces.

##### Template Method

Purpose & Pros:

* ✔️ Template Method defines the skeleton of an algorithm in the superclass but lets subclasses override specific steps of the algorithm without changing its structure. The Template Method lets you turn a monolithic algorithm into a series of individual steps which can be easily extended by subclasses while keeping intact the structure defined in a superclass. Use the Template Method pattern when you want to let clients extend only particular steps of an algorithm, but not the whole algorithm or its structure.
* ✔️ Typically, the Template Method DP is relevant to implement when you have several classes that contain almost identical algorithms with some minor differences: When you turn such an algorithm into a template method, you can pull up the steps with similar implementations into a superclass, eliminating code duplication and the need to modify all classes when the algorithm changes. Consider making the template method `final` to prevent subclasses from overriding it, as well as the so-called “_abstract steps_” to be implemented by every subclass, while the so-called “_optional steps_” are partly or wholly open to redefinition (Single Responsibility Principle), as they are specific to each subclass.
* ✔️ You may also want to insert the so-called “_hooks_” — optional empty methods placed before and after any crucial steps. Thus subclasses are provided with additional extension points (Open/Closed Principle).

Limits & Cons:

* ❌ Template methods tend to be harder to maintain the more steps they have.
* ❌ Some clients may be limited by the provided skeleton of an algorithm.
* ❌ You might violate the Liskov Substitution Principle by suppressing a default step implementation via a subclass.

Relations with Other Patterns:

* 🔀 [Factory Method](#factory-method-aka-virtual-constructor) is a specialization of Template Method. At the same time, a Factory Method may serve as a step in a large Template Method.
* 🔀 While Template Method is based on inheritance —for it lets you alter parts of an algorithm by extending those parts in subclasses— [Strategy](#strategy) is based on composition: you can alter parts of the object’s behavior by supplying it with different strategies that correspond to that behavior.
* 🔀 Template Method works at the class level, so it’s static. [Strategy](#strategy) works on the object level, letting you switch behaviors at runtime.

##### Visitor

Purpose & Pros:

* ✔️ _

Limits & Cons:

* ❌ _

#### Python-Specific Design Patterns

Source: https://python-patterns.guide

* Gang of Four: Principles
* Python-Specific Patterns
* Gang of Four: Creational Patterns
* Gang of Four: Structural Patterns
* Gang of Four: Behavioral Patterns

### Software Architectures

* **_n_-Tier Architecture:**
* **Microservices Architecture:**
* **Hexagonal Architecture:**
    * Source: https://blog.octo.com/architecture-hexagonale-trois-principes-et-un-exemple-dimplementation/

## Test Driven Development, aka TDD

* **Given-When-Then** approach (see below)
* **Red, Green, Refactor** methodology:
    * Write test (obviously red)
    * Make it pass to green by implementing the tested method
    * Iterate until all tests pass
    * Finally, refactor and optimize the tested method

### Unit Tests

* **First purpose:** to avoid code regression
* **Scope:** test only a given _unit_ —a class, a method, or a behavior— and nothing beyond, hence the name
* **Method:**
    * Follow the F.I.R.S.T. rules: Unit tests must be
        * `F`ast
        * `I`solate
        * `R`epeatable (or Reusable)
        * `S`elf-Verifying
        * `T`imely
* **Code coverage:** 
    * code coverage ≠ code quality: code coverage is necessary but not sufficient (because the tests may be poorly written)
* **Branch coverage:**
    * In complex methods/functions, the logic can follow different paths, all of which must be equally tested. See https://stackoverflow.com/questions/35034977/what-is-a-branch-in-code-coverage-for-javascript-unit-testing
* **Mock vs Dummy vs Stub vs Fake object:**
    * Discussion: https://stackoverflow.com/questions/3459287/whats-the-difference-between-a-mock-stub
    * According to Martin Fowler's article:
        * **Dummy objects** are passed around but never actually used. Usually they are just used to fill parameter lists.
        * **Fake objects** actually have working implementations, but usually take some shortcut which makes them not suitable for production (an in memory database is a good example).
        * **Stubs** provide canned answers to calls made during the test, usually not responding at all to anything outside what's programmed in for the test. Stubs may also record information about calls, such as an email gateway stub that remembers the messages it 'sent', or maybe only how many messages it 'sent'.
        * **Mocks** are what we are talking about here: objects pre-programmed with expectations which form a specification of the calls they are expected to receive.
* **Given-When-Then, or GWT:**
    * Proposed by Dan North as part of behavior-driven development. See https://en.wikipedia.org/wiki/Given-When-Then
    * **Given** describes the preconditions and initial state before the start of a test and allows for any pre-test setup that may occur. 
    * **When** describes actions taken by a user during a test. 
    * **Then** describes the outcome resulting from actions taken in the when clause.

### BDD & ATDD
* Source: https://www.browserstack.com/guide/tdd-vs-bdd-vs-atdd
* **Behavioral-Driven Development (BDD)** derives from the TDD, uses GWT, and defines various ways to develop a feature based on its behavior.
    * Key benefits:
        * Helps reach a wider audience by the usage of non-technical language
        * Focuses on how the system should behave from the customer's and the developer's perspective
        * BDD is a cost-effective technique
        * Reduces efforts needed to verify any post-deployment defects
* **Acceptance Test-Driven Development (ATDD):** while BDD focuses more on the behavior of the feature, the ATDD focuses on capturing the accurate functional requirements (real-world scenarios), stated from the user's perspective.
    * Typically, there is one single acceptance test, written in pair with the PO, and it is larger.
    * Key benefits:
        * Requirements are very clearly analyzed without any ambiguity
        * Encourages collaboration among cross-team members
        * The acceptance test serves as a guide for the entire development process

## Git

### Main concepts

Sources:

* _Pro Git_ book, 2nd ed.: https://git-scm.com/book/en/v2
* _LearnGitBranching_ interactive online course: https://learngitbranching.js.org/

### Best practices

Source: _Git Style Guide_, https://github.com/agis/git-style-guide

# II — Personal wishlist

## Évitez d'utiliser les variables globales

L'utilisation de variables globales, bien que celle-ci paraisse logique aux développeurs débutants, complique le déboggage, empêche d'établir la précédence des fonctions qui opèrent sur celles-ci, etc. [À éviter à tout prix.](https://google.github.io/styleguide/pyguide.html#25-global-variables)

## Utilisez des fonctions pures et parfaites

À l'exception des fonctions qui sont des méthodes de classe (en programmation orientée objet) et qui opèrent sur des variables (attributs) de classe, cette règle concerne néanmoins toute fonction "utilitaire" réutilisable. Cette règle est aussi propre à un paradigme dit programmation fonctionnelle :

* Une fonction ne doit pas modifier des variables définies à l'extérieur du corps de cette fonction. Autrement dit, une fonction ne doit opérer que sur les variables qu'elle reçoit en paramètre ou des variables qu'elle crée.
* Une fonction doit forcément retourner une valeur afin que les fonctions puissent être enchaînées comme ceci : `extract_content(url).parse(expected_format).for_each(convert_to, whatever)`.

## Conservez les constantes dans un fichier dédié et importable

Les variables globales qui n'ont vocation qu'à être lues et non pas à être modifiées, sont des constantes. Mettez toutes les constantes dans un fichier à part et importez-les au besoin. Garder toutes les constantes au même endroit facilite grandement la maintenance.

## Privilégiez le couplage faible, évitez le couplage fort

Chaque fichier de code doit avoir un rôle et un (seul) langage à l'intérieur. Mettre du SQL en dur dans un fichier Python constitue ce qu'on appelle le couplage fort : lorsqu'on aura besoin de modifier la partie SQL, c'est le fichier Python qui sera modifié (alors même que la modification n'aura rien à voir avec le code Python) ; on dit dès lors que les fichiers sont fortement couplés et il s'agit d'une pratique à éviter.

La bonne pratique, dite la couplage faible, consiste à séparer les fichiers selon leur rôle et permettre à ceux-ci de s'importer les uns les autres via des instructions comme `source some_file.sh` (ou son raccourci `. some_file.sh`) en Shell, ainsi que `import some_package` / `from some_package import some_module` et `with open(file, mode) as...` en Python.

### <span style="color:red">**❌ À ne pas faire**</span>

Un fichier Python contenant du SQL en dur :
```python
def RGCEC024(entite):
    Part1 = str("""INSERT INTO """ + prefixe_work + """.MDC_POC_ANO_TEST_A
(...)
    FROM
        """ + prefixe_stg + """.V_ECHEANCIER_C_""" + entite + """ a
    INNER JOIN """ + prefixe_work + """.mdc_sar b
(...)
    """)
    return Part1
```

### <span style="color:green">**✔️ À faire**</span>

1. Je crée un fichier distinct, contenant la requête SQL et des variables Python interpolées (aka f-string), incrustées dans le corps de la requête :
```sql
INSERT INTO {prefixe_work}.MDC_POC_ANO_TEST_A
(...)
    FROM
        {prefixe_stg}.V_ECHEANCIER_C_{entite} a
    INNER JOIN {prefixe_work}.mdc_sar b
(...)
```
2. Le fichier Python retrouve et lit le fichier SQL, et valorise les variables que ce dernier contient :
```python
def RGCEC024(entite, prefixe_work, prefixe_stg):
    path_to_sql_template = 'RGCEC024.sql'
    template_content_raw = ''
    try:
        with open(file=path_to_sql_template, mode='r') as f:
            template_content_raw = f.read()
    except FileNotFoundError as e:
        print(f"Error: SQL template '{path_to_sql_template}' not found", str(e), sep='\n')
    try:
        Part1 = eval('f"""' + template_content_raw + '"""')
        return Part1
    except Exception as e:
        print(f"An error occurred while trying to fill the SQL template {path_to_sql_template!r} with parameters", str(e), sep='\n')
```


## Évitez le code dupliqué

* Toute valeur en dur, du moment qu'elle revient au moins deux fois, doit devenir une variable et être réutilisée en tant que telle.
* Toute instruction ou ensemble d'instructions, du moment qu'ils ont lieu au moins deux fois, doivent devenir une fonction (idéalement, générique) et être réutilisés en tant que tels.
* Lorsqu'une pile de `if...elif...elif...elif...` commence manifestement à grandir, pensez à la factoriser avec, par exemple, un dictionnaire. C'est the Pythonic way d'implémenter un `case...when` switcher:
```python
def calculate(n1: int, n2: int, operator: str) -> func:
    switcher = {
        '+': lambda n1, n2: n1 + n2,
        '-': lambda n1, n2: n1 - n2,
        '*': lambda n1, n2: n1 * n2,
        '/': lambda n1, n2: n1 / n2,
    }
    return switcher.get(operator)(n1,n2)

print(calculate(10, 20, '+'))
print(calculate(10, 20, '-'))
print(calculate(10, 20, '*'))
print(calculate(10, 20, '/'))
```

## *Code style* : Standardisez les conventions de nommage

Il s'agit d'en choisir une pour les constantes, une pour les variables et une pour les fonctions. Le but est simplement d'éviter que le code ressemble à un marché de quartier en début d'après-midi.

Les règles de nommage communément admises sont :
* `lowercase_snake_case`
    * pour toutes les variables, fonctions et méthodes de classe en Python ;
* `UPPERCASE_SNAKE_CASE`
    * pour toutes les constantes en Java, Python, JavaScript et d'autres langages, et également les variables d'environnement en Shell ;
* `camelCase`
    * pour les variables, attributs de classe et méthodes de classe en Java, JavaScript et d'autres ;
* `PascalCase`
    * pour les noms de classes d'objets en Python et Java, et et les interfaces en Java ;
* `lowercase-kebab-case`
    * pour les branches Git
* `UPPERCASE-KEBAB-CASE`
    * ...

## *Code style* : Nommez vos fichiers, vos variables et fonctions de façon la plus explicite possible

Ceci aussi bien pour un collègue qui vient d'intégrer le projet que pour vous-même dans quelques mois (oui oui, avec le temps on oublie même son propre code, ne vous faites pas d'illusions).

## Quelques exemples

Un exemple en Shell :
### <span style="color:red">**❌ À ne pas faire**</span>
```sh
. fonc2.sh
FILE =flux.gzip
Fichier_param= /path/to/some/file.cfg
. CltWgt ${Fichier_param} ${FILE}
CR=$?
if [ $CR -ne 0 ]
then
    echo "Some error message"
    exit $CR
else
    . CtrlWgt ${FILE}
    CR=$?
    if [ $CR -ne 0 ]
    then
        echo "Another error message"
exit $CR
```

### <span style="color:green">**✔️ À faire**</span>
```sh
. functions.sh
file_name_for_received_file=flux_wgt.gzip
path_to_fichier_param=/path/to/some/file.cfg
. collecte_flux_wgt ${path_to_fichier_param} ${file_name_for_received_file}
code_retour_collecte_flux_wgt=$?
if [ $code_retour_collecte_flux_wgt -ne 0 ]
then
    echo "Some error message"
    exit $code_retour_collecte_flux_wgt
else
    . controle_flux_wgt ${file_name_for_received_file}
    code_retour_controle_flux_wgt=$?
    if [ $code_retour_controle_flux_wgt -ne 0 ]
    then
        echo "Another error message"
exit $code_retour_controle_flux_wgt
```

<br>

Un exemple en Python :

### <span style="color:red">**❌ À ne pas faire**</span>
```python
pln_eur = 0.210

def convert2eur(sum):
    return sum * rub_eur

plnInUsd = 0.214

def convert_to_usd(sum):
    return sum * plnInUsd

in_eur = convert2eur(500)
inUsd = convert_to_usd(500)
```

### <span style="color:green">**✔️ À faire**</span>
```python
# part of data.py
⋮
currency_ratios = {
    ['PLN']: {
        ['EUR']: 0.210,
        ['USD']: 0.214,
        ⋮
    }
}
⋮
```

```python
from data import currency_ratios

pln_eur_ratio = get_currency_ratio('PLN', 'EUR', currency_ratios)
pln_usd_ratio = get_currency_ratio('PLN', 'USD', currency_ratios)
amount_in_pln = 500
amount_in_eur = convert_currency(pln_eur_ratio, amount_in_pln)
amount_in_usd = convert_currency(pln_usd_ratio, amount_in_pln)
```

```python
def get_currency_ratio(source_currency: str = '', target_currency:str = '', ratios_data_source: dict) -> float:
    return ratios_data_source[source_currency][target_currency]

def convert_currency(ratio: float, source_amount: float) -> float:
    target_amount = source_amount * ratio
    return target_amount
```